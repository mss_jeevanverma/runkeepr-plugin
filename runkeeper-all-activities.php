<?php
/*
Plugin Name: Runkeeper Get All Activities
Plugin URI: http://example.com
Description: Runkeeper Get All Activities
Version: 1.0
Author: Vikrant Vir Bhalla
Author URI: http://w3guy.com
*/

ini_set("display_errors", "1");
error_reporting(E_ALL);
session_start();
include("sdk_runkeepr/vendor/autoload.php");
//include('sdk_runkeepr/config/conf.php');

use \HealthGraph\Authorization;
use \HealthGraph\Client;
use \HealthGraph\HealthGraphClient;

$redirect_url = "http://".$_SERVER["SERVER_NAME"].$_SERVER['REQUEST_URI'];
define("client_id", "6be285374e1742659525d2b72fc86991");
define("client_secret", "1a172919cd7a41459be54b80b9df8228");
define("redirect_url", $redirect_url);

if (isset($_SESSION['token'])) {
  $token = $_SESSION['token'];
}



    
function html_form_code() {
    
    if(isset($_GET['pro_id'])) {
        get_user_view($_GET['pro_id']);
        $onprofile = "yes";
    }
    if(@$onprofile != "yes"){

        if (!isset($token)&& !isset($_GET['code'])) {
            $button = Authorization::getAuthorizationButton(client_id, redirect_url);
            echo $button['html'];
            
        }elseif(isset($_GET['code'])){
            get_user_details_to_db($_GET['code']);
        } 

    }
    

}

function get_user_view($pro_id){
    global $wpdb;
    $get_profile = $wpdb->get_results("SELECT * FROM rk_profile WHERE profile_id = '$pro_id'");
    if(!count($get_profile)) { die("No Data Found ..."); } 
    $profile = $get_profile[0];
    include("templates/get_user_view.php");
   

} // function -> get_user_view

function get_user_details_to_db($code){
    global $wpdb;
    global $current_user;
    get_currentuserinfo();
    $wp_un =  $current_user->user_login ;
    $wp_email =  $current_user->user_email ;


    $ret_url = "http://".$_SERVER["SERVER_NAME"].$_SERVER['REQUEST_URI'];
    $arr = explode("?",$ret_url);
    $ret_url = $arr[0];

    $token = Authorization::authorize($code, client_id, client_secret, $ret_url );
    //$token = Authorization::authorize($code, "6be285374e1742659525d2b72fc86991", "1a172919cd7a41459be54b80b9df8228", "http://mastersoftwaretechnologies.com/runkeeper/run/" );

    $hgc = HealthGraphClient::factory();

    $hgc->getUser(array(
        'access_token' => $token['access_token'],
        'token_type' => $token['token_type'],
    ));
	
    $profile = $hgc->GetProfile();
    $profile_data = $profile->toArray();

    $birthday = $profile_data['birthday'];
    $elite = $profile_data['elite'];
    $medium_picture = $profile_data['medium_picture'];
    $gender = $profile_data['gender'];
    $profile_url = $profile_data['profile'];
    $name = $profile_data['name'];
    $location = $profile_data['location'];
    $normal_picture = $profile_data['normal_picture'];
    $profile_id = substr($profile_url, strrpos($profile_url, '/') + 1);

    $get_activities = $hgc->getIterator('GetFitnessActivityFeed');
    $get_activities_result = $get_activities->toArray();

    $wpdb->delete( "rk_profile", array( 'profile_id' => $profile_id ) );
    $profile_sql = "INSERT INTO rk_profile (birthday, elite, medium_picture, gender, profile, name, location, normal_picture, profile_id,email)
VALUES ('$birthday', '$elite', '$medium_picture', '$gender', '$profile_url', '$wp_un', '$location', '$normal_picture','$profile_id','$wp_email')";
    $wpdb->query($profile_sql); 

    $wpdb->delete( "rk_activities", array( 'profile_id' => $profile_id ) );
    foreach ($get_activities_result as $activity){

        $duration = $activity['duration'];
        $start_time = $activity['start_time'];
        $total_calories = $activity['total_calories'];
        $total_distance = $activity['total_distance'];
        $entry_mode = $activity['entry_mode'];
        $has_path = $activity['has_path'];
        $source = $activity['type'];
        $type = $activity['duration'];
        $uri = $activity['uri'];
        
        $get_path = $hgc->getCommand('GetFitnessActivity', array('uri' => $uri));
        $result = $get_path->execute()->get('path');
        if(count($result)){
            $path_of_activity = json_encode($result);
        }else{
            $path_of_activity ='';
        }
       
        $activity_sql = "INSERT INTO rk_activities (profile_id, duration, start_time, total_calories, total_distance, entry_mode, has_path, source, type, uri, path_of_activity)
    VALUES ('$profile_id', '$duration', '$start_time', '$total_calories', '$total_distance', '$entry_mode', '$has_path', '$source','$type','$uri', '$path_of_activity' )";
    
        $wpdb->query($activity_sql);
    }

    $ret_url_new = $ret_url."?pro_id=$profile_id";

    echo "<meta http-equiv='refresh' content='0; URL=$ret_url_new'>";
    exit;

} 

function cf_shortcode() {
    ob_start();
    html_form_code();
    return ob_get_clean();
}
 
add_shortcode( 'sitepoint_contact_form', 'cf_shortcode' );


?>
