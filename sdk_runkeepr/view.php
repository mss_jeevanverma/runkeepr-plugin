<?php 
	ini_set("display_errors", "1");
	error_reporting(E_ALL);

	include('config/conf.php');
	$profile_id  = $_GET['profile_id'];
	$get_profile_data = "SELECT * FROM rk_profile WHERE profile_id = '$profile_id' ";
	$get_all_activities = "SELECT * FROM  rk_activities WHERE profile_id = '$profile_id' ";

	$profile_data = $conn_dbb->query($get_profile_data);
	$profile = $profile_data->fetch_assoc();
	$activies_data = $conn_dbb->query($get_all_activities);

?>
<style>

.row{
	margin-top: 10px;
}

</style>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>

<!--code for map start-->
<style type="text/css">
    html { height: 100% }
    body { height: 100%; margin: 0; padding: 0 }
    .map { height: 90%; width: 90% }
</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry"></script>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script-->
    
<script type="text/javascript">
var line;

var map;
var pointDistances;

function initialize(map_no) {
	
    var mapOptions = {
        
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('map_'+map_no), mapOptions);

    var line_vals = new google.maps.LatLng(30.66048, 76.860289) +'#'+new google.maps.LatLng(30.656759, 76.815157);
    //console.log(line_vals);
    //for(var i =0 ; i<2 ;i++){

    //}

    var path_data =  JSON.parse($("#path_data_"+map_no).text());

    var lineCoordinates = [];
    
    for(var ll=0;ll<path_data.length;ll++)
    {
    	//alert(path_data[ll].latitude);
    	lineCoordinates[ll] = new google.maps.LatLng(path_data[ll].latitude, path_data[ll].longitude) ;
    }
    map.setCenter(lineCoordinates[0]);
    
    // point distances from beginning in %
    var sphericalLib = google.maps.geometry.spherical;

    pointDistances = [];
    var pointZero = lineCoordinates[0];
    var wholeDist = sphericalLib.computeDistanceBetween(
                        pointZero,
                        lineCoordinates[lineCoordinates.length - 1]);
    
    for (var i = 0; i < lineCoordinates.length; i++) {
        pointDistances[i] = 100 * sphericalLib.computeDistanceBetween(
                                        lineCoordinates[i], pointZero) / wholeDist;
        console.log('pointDistances[' + i + ']: ' + pointDistances[i]);
    }

    // define polyline
    var lineSymbol = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 6,
        strokeColor: '#393'
    };

    line = new google.maps.Polyline({
        path: lineCoordinates,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        icons: [{
            icon: lineSymbol,
            offset: '100%'
        }],
        map: map
    });

    animateCircle();
}


var id;
function animateCircle() {
    var count = 0;
    var offset;
    var sentiel = -1;
    
    id = window.setInterval(function () {
        count = (count + 1) % 200;
        offset = count /2;
        
        for (var i = pointDistances.length - 1; i > sentiel; i--) {
            if (offset > pointDistances[i]) {
                console.log('create marker');
                var marker = new google.maps.Marker({
                    icon: {
                        url:"https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle_blue.png",
                        size: new google.maps.Size(7,7),
                        anchor: new google.maps.Point(4,4)
                    },
                    position: line.getPath().getAt(i),
                    title: line.getPath().getAt(i).toUrlValue(6),
                    map: map
                });
                
                sentiel++;
                break;
            }
        }
        
        // we have only one icon
        var icons = line.get('icons');
        icons[0].offset = (offset) + '%';
        line.set('icons', icons);
        
        if (line.get('icons')[0].offset == "99.5%") {
            icons[0].offset = '100%';
            line.set('icons', icons);
            window.clearInterval(id);
        }
        
    }, 20);
}

//google.maps.event.addDomListener(window, 'load', initialize);
//initialize();
</script>
<!-- code for map Ende -->

</head>
<body>

<div class="container">
  <h1>Timline Data</h1>
  
  <div class = "row" >
	  <div class="col-md-8" >
	  	<center><img src = "<?php echo $profile['normal_picture']; ?>" /></center> 		
	  </div>
	  <div class="col-md-4" >
	  	<h2><?php echo $profile['name']; ?></h2>
	  </div>
  </div>

  <div class="row">
  
  <?php 
  $loop_i = 0;
  	while($activity = $activies_data->fetch_assoc()){
		$seconds = $activity['duration'];
		$hours = floor($seconds / 3600);
		$mins = floor(($seconds - ($hours*3600)) / 60);
		$secs = floor($seconds % 60);
		

	?>
    <div class="col-md-12" style="background-color:lavender;">
    	<h3><b>Start Time</b> :- <?php echo $activity["start_time"]; ?> </h3>
    	<h4><b>Distance Ran</b> :- <?php echo number_format($activity["total_distance"],2)." KM"; ?> </h4>
    	<h4><b>Duration</b> :- <?php echo $hours.":".$mins.":".$secs." Hours "; ?> </h4>
    	<span id="<?php echo "path_data_".$loop_i; ?>" style="display:none;"><?php echo $activity['path_of_activity']; ?></span>
   		
    </div>
    <?php 
    if(strlen($activity['path_of_activity'])>5){ ?>
			
    <div id='map_<?php echo $loop_i; ?>' class="map"> </div> 
   	<script>initialize(<?php echo $loop_i; ?>);</script>
   	<?php } ?>
    <div class="col-xs-12" style="height:50px;"></div>
    
    <?php $loop_i++; } ?>
    
  </div>

</div>

</body>
</html>



