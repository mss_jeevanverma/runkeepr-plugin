<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script>

        function initialize(id) {
          var start = JSON.parse($("#start-"+id).text());  
          var end = JSON.parse($("#end-"+id).text());  

          var mapOptions = {
            zoom: 12,
            center: new google.maps.LatLng(start.latitude, start.longitude),
            mapTypeId: google.maps.MapTypeId.TERRAIN
          };


          var map = new google.maps.Map(document.getElementById('map-canvas-'+id),
              mapOptions);

          var flightPlanCoordinates = [];
          flightPlanCoordinates[0] = new google.maps.LatLng(start.latitude, start.longitude) ;
          flightPlanCoordinates[1] = new google.maps.LatLng(end.latitude, end.longitude) ;
            new google.maps.Marker({
        position: new google.maps.LatLng(start.latitude, start.longitude), map: map,title: 'Start'
                                });
            new google.maps.Marker({
        position: new google.maps.LatLng(end.latitude, end.longitude), map: map,title: 'End'
                                });
  
          var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
          });


          flightPath.setMap(map);
          
        }

        </script>


        <center><div style="border-radius: 50%;height: 129px;overflow: hidden;width: 196px;" ><img src = "<?php echo $profile->normal_picture; ?>" /></div>      

        <h2><?php echo $profile->name; ?></h2></center>

    <?php 

    $activies_data = $wpdb->get_results("SELECT * FROM  rk_activities WHERE profile_id = '$pro_id' AND source IN ('Cycling','Walking','Running')");

    foreach($activies_data as $index => $activity){
        $seconds = $activity->duration;
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours*3600)) / 60);
        $secs = floor($seconds % 60);
        if(strlen($activity->path_of_activity)>5){
            $path_of_activity = json_decode($activity->path_of_activity,2);
            $path_start_point = current($path_of_activity);
            unset($path_start_point['altitude']);unset($path_start_point['type']);unset($path_start_point['timestamp']);            
            $path_end_point = end($path_of_activity);
            unset($path_end_point['altitude']);unset($path_end_point['type']);unset($path_end_point['timestamp']);
        }
        $activity_date = strtotime($activity->start_time);
        $date_block = strtotime("Thu, 10 jul 2015 13:38:43");
        if($activity_date<$date_block){continue;}
    	if(number_format($activity->total_distance,2)<600 && $mins<10  ){
    		continue;
    	}

    ?>
    <div style="border: 1px groove;overflow:auto;margin:0 auto;width:100%;">
            <?php if(strlen($activity->path_of_activity)>5){ ?>

            <div style="float: left; width: 50%;">
                <span id="start-<?php echo $index; ?>"  style="display:none;"><?php print_r(json_encode($path_start_point)); ?></span>
                <span id="end-<?php echo $index; ?>"  style="display:none;"><?php print_r(json_encode($path_end_point)); ?></span>
                <div id="map-canvas-<?php echo $index; ?>" style="height:300px;border;groove;"></div>

                <script> $(document).ready(function(e){initialize(<?php echo $index; ?>);}); </script>
            </div>   

            <?php } ?>
        <div  style="padding:25px;float:left;width:50%;height:300px">

            <p><b>Start Time :- <?php echo $activity->start_time; ?> </b></p>
            <p><b>Distance Ran :- <?php echo number_format($activity->total_distance,2)." KM"; ?> </b></p>
            <p><b>Duration :- <?php echo $hours.":".$mins.":".$secs." Hours "; ?> </b></p>
            <p><b>Type :- <?php echo $activity->source; ?> </b></p>
       
        </div>
    </div> 
    
    <div style="height:50px;"></div>

    <?php  } // for each    